import { Repository, EntityRepository, Brackets } from "typeorm";

import { GiaoVien } from "../entity/GiaoVien";
import { IGiaoVien, IGiaoVienInput,IGiaoVienUpdate } from "../interfaces/iGiaoVien";


@EntityRepository(GiaoVien)
export class GiaoVienRepository extends Repository<GiaoVien>{

    public async getAllGiaoVien(): Promise<GiaoVien[]> {
        return await this.createQueryBuilder("GiaoVien").getMany();
    }
    public async createGV(gv: IGiaoVienInput) {
        return await this.createQueryBuilder().insert().into("GiaoVien").values({
            "Ten": gv.Ten,
            "Luong": gv.Luong,
            "NgaySinh": gv.NgaySinh
        }).execute();
    }
    public async updateGV(gv:IGiaoVienUpdate){

    }
    public async getGiaoVienById(id: number): Promise<GiaoVien> {
        return await this.createQueryBuilder("GiaoVien").where("GiaoVien.MaGV = :id", { id: id }).getOne();
    }
    public async getGiaoVienByName(name: string): Promise<GiaoVien> {
        // return await this.createQueryBuilder().select("GiaoVien").from(GiaoVien,"gv").where("gv.Ten =:Ten",{Ten:name});
        return await this.createQueryBuilder().select().where("GiaoVien.Ten =:Ten").setParameter("Ten", name).getOne();
    }
    public async getArrayGiaoVien(name1: string, name2: string, name3: string): Promise<GiaoVien[]> {
        return await this.createQueryBuilder().select().where("GiaoVien.Ten IN (:...names)", { names: [name1, name2, name3] }).getMany();
    }
    public async getGiaoVienByNameAndSalary(name: string, salary: number): Promise<GiaoVien[]> {
        return await this.createQueryBuilder("GiaoVien").where("GiaoVien.Ten=:name1", { name1: name }).andWhere("GiaoVien.Luong =:luong", { luong: salary }).getMany();
    }
    public async getGiaoVienBySalaryAndNameOrName(salary: number, name1: string, name2: string): Promise<GiaoVien[]> {
        return await this.createQueryBuilder("GiaoVien").where("GiaoVien.luong = :luong", { luong: salary }).andWhere(new Brackets(qd => {
            qd.where("GiaoVien.Ten = :ten1", { ten1: name1 }).orWhere("GiaoVien.ten = :ten2", { ten2: name2 })
        })).getMany();
    }

    public async orderByGiaoVien(): Promise<GiaoVien[]> {
        return await this.createQueryBuilder("GiaoVien").orderBy("GiaoVien.Luong", "DESC").getMany();
    }

    // public async groupByLuong():Promise<GiaoVien[]>{
    //     return await this.createQueryBuilder("GiaoVien").groupBy("GiaoVien.Luong").getMany();
    // }

    public async limitGiaoVien(): Promise<GiaoVien[]> {
        return await this.createQueryBuilder("GiaoVien").limit(5).getMany();
    }


    public async offsetGiaoVien(): Promise<GiaoVien[]> {
        return await this.createQueryBuilder("GiaoVien").offset(2).limit(2).getMany();
    }

    public async leftJoinGiaoVienAndNguoiThan(): Promise<GiaoVien[]> {
        return await this.createQueryBuilder("GiaoVien").leftJoinAndSelect("GiaoVien.Nguoithan", "NguoiThan").where("GiaoVien.Ten = :ten", { ten: "Le Van Quyen" }).getMany();
    }
    public async leftJoinGVandNTnoSelect(): Promise<GiaoVien[]> {
        return await this.createQueryBuilder("GiaoVien").leftJoinAndSelect("GiaoVien.Nguoithan", "NguoiThan").where("GiaoVien.Ten IN (:...names)", { names: ["Le Van Quyen", "Pham Nhat Vuong"] }).getMany();
    }

}