import {Request,Response,NextFunction} from 'express';
import moment from 'moment';
import {Container} from 'typedi';
import { GiaoVien } from '../../entity/GiaoVien';
import { GiaoVienServices } from '../../services/GiaoVien.services';

export class GiaoVienController{
    public async getAllGiaoVien(req:Request, res:Response,next :NextFunction): Promise<Response|void>{
        try{
            const gvServices=Container.get(GiaoVienServices);
            const gvs=await gvServices.getAllGiaoVien();
            return res.status(200).json(gvs);
            
        }catch(e){
            console.log(e);
        }
    }
    public async createGV(req:Request,res:Response,next:NextFunction):Promise<Response|void> {
        try{
            const gv=new GiaoVien();
            const {Ten,Luong,NgaySinh}=req.body;
            const date=NgaySinh.split('-');
            date[2]=parseInt(date[2])+1;
            const ngay=date.join('-');
            const mydate=moment(ngay,"YYYY-MM-DD").isValid();
            if(!mydate){
               return res.send("Date Wrong");
            }
            const mydate1=moment(ngay,"YYYY-MM-DD").toDate();
            console.log(mydate1);
            gv.Ten=Ten;
            gv.Luong=Luong;
            gv.NgaySinh=mydate1;
            const gvServices=Container.get(GiaoVienServices);
            const gvs=gvServices.createGV(gv);
            return res.status(200).json(gvs);
            
        }catch(e){

        }
    }
    public async getGiaoVienById(req:Request,res:Response,next:NextFunction) :Promise<Response | void>{
        try{
            const id=parseInt(req.params.id);
            const gvServices=Container.get(GiaoVienServices);
            const gvs=await gvServices.getGiaoVienById(id);
            return res.status(200).json(gvs);
        }catch(e){

        }
    }

    public async getGiaoVienByName(req:Request,res:Response,next:NextFunction):Promise<Response|void>{
        try{
            const name=req.params.name;
            const gvServices=Container.get(GiaoVienServices);
            const gvs=await gvServices.getGiaoVienByName(name);
            return res.status(200).json(gvs);
        }catch(e){
            console.log(e);
        }
    }
    public async getArrayGiaoVien(req:Request,res:Response,next:NextFunction):Promise<Response|void>{
        try{
            
            const {name1,name2,name3}=req.params;
            const gvServices=Container.get(GiaoVienServices);
            const gvs= await gvServices.getArrayGiaoVien(name1,name2,name3);
            return res.status(200).json(gvs);

        }catch(e){
            console.log(e);
        }
    }
    public async getGiaoVienByNameAndSalary(req:Request, res:Response,next:NextFunction){
        try{
            const {name}=req.params;
            const salary=parseInt(req.params.salary);
            
            const gvServices=Container.get(GiaoVienServices);
            const gvs=await gvServices.getGiaoVienByNameAndSalary(name,salary);
            return res.status(200).json(gvs);
        }catch(e){
            console.log(e);
        }
    }
    public async getGiaoVienBySalaryAndNameOrName(req:Request, res:Response,next:NextFunction){
        try{
            const {name1,name2}=req.params;
            const salary=parseInt(req.params.salary);
            const gvServices=Container.get(GiaoVienServices);
            const gvs=await gvServices.getGiaoVienBySalaryAndNameOrName(salary,name1,name2);
            return res.status(200).json(gvs);
        }catch(e){
            console.log(e);
        }
    }
    public async orderByGiaoVien(req:Request, res:Response,next:NextFunction){
        try{
            const gvServices=Container.get(GiaoVienServices);
            const gvs=await gvServices.orderByGiaoVien();
            return res.status(200).json(gvs);
        }catch(e){
            console.log(e);
        }
    }
    // public async groupByLuong(req:Request, res:Response,next:NextFunction){
    //     try{
    //         const gvServices=Container.get(GiaoVienServices);
    //         const gvs=await gvServices.groupByLuong();
    //         return res.status(200).json(gvs);
    //     }catch(e){
    //         console.log(e);
    //     }
    // }

    public async limitGiaoVien(req:Request, res:Response,next:NextFunction){
        try{
            const gvServices=Container.get(GiaoVienServices);
            const gvs=await gvServices.limitGiaoVien();
            return res.status(200).json(gvs);
        }catch(e){
            console.log(e);
        }
    }

    public async offsetGiaoVien(req:Request, res:Response,next:NextFunction){
        try{
            const gvServices=Container.get(GiaoVienServices);
            const gvs=await gvServices.offsetGiaoVien();
            return res.status(200).json(gvs);
        }catch(e){
            console.log(e);
        }
    }
    public async leftJoinGiaoVienAndNguoiThan(req:Request, res:Response,next:NextFunction){
        try{
            const gvServices=Container.get(GiaoVienServices);
            const gvs=await gvServices.leftJoinGiaoVienAndNguoiThan();
            return res.status(200).json(gvs);
        }catch(e){
            console.log(e);
        }
    }
    public async leftJoinGVandNTnoSelect(req:Request, res:Response,next:NextFunction){
        try{
            const gvServices=Container.get(GiaoVienServices);
            const gvs=await gvServices.leftJoinGVandNTnoSelect();
            return res.status(200).json(gvs);
        }catch(e){

        }
    }
}