import {Router} from 'express';
import giaovien from './router/GiaoVien.Router';

export default ()=>{
    const app=Router();
    giaovien(app);

    return app;

}