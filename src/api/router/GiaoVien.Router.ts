import {Router} from 'express';
import { GiaoVienController } from '../controler/GiaoVien.controller';

const router=Router();

export default (app:Router):void=>{
    console.log("vao router");
    app.use('/giaovien',router);

    //get all giao vien
    router.get('/',new GiaoVienController().getAllGiaoVien);
    router.post('/creategv/',new GiaoVienController().createGV);

    //orderBy GiaoVien By Luong
    router.get("/order/",new GiaoVienController().orderByGiaoVien);
    // router.get('/groupby/',new GiaoVienController().groupByLuong);
    //limit giao vien
    router.get("/limitgv/",new GiaoVienController().limitGiaoVien);


    //offset
    router.get("/offsetgv/",new GiaoVienController().offsetGiaoVien);

    //left Giao Vien voi Nguoi Than
    router.get("/leftgvandnt/",new GiaoVienController().leftJoinGiaoVienAndNguoiThan);


    router.get("/leftgvandntnoselect/",new GiaoVienController().leftJoinGVandNTnoSelect);
    // get giao vien theo id
    router.get('/:id',new GiaoVienController().getGiaoVienById);

    //get giao vien theo ten
    router.get('/:name',new GiaoVienController().getGiaoVienByName);
    //get giao vien theo danh dach ten
    router.get('/:name1/:name2/:name3',new GiaoVienController().getArrayGiaoVien);

    //get giao vien theo ten va luong
    router.get('/:name/:salary',new GiaoVienController().getGiaoVienByNameAndSalary);
    

    //get giao vien co luong la 10000 va ten bang Quan hoac Tai
    router.get("/:salary/:name1/:name2",new GiaoVienController().getGiaoVienBySalaryAndNameOrName);

}