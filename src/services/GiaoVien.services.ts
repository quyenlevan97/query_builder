import { Service } from "typedi";
import { InjectRepository } from 'typeorm-typedi-extensions'
import { GiaoVienRepository } from "../repositories/GiaoVien.repos";
import { GiaoVien } from "../entity/GiaoVien";
import { IGiaoVienInput } from "../interfaces/iGiaoVien";
@Service()
export class GiaoVienServices {
    constructor(
        @InjectRepository() private giaovienRepository: GiaoVienRepository
    ) {

    }
    public async getAllGiaoVien(): Promise<GiaoVien[]> {
        return await this.giaovienRepository.getAllGiaoVien();
    }
    public async createGV(gv: IGiaoVienInput) {
        return await this.giaovienRepository.createGV(gv);
    }
    public async getGiaoVienById(id: number): Promise<GiaoVien> {
        return await this.giaovienRepository.getGiaoVienById(id);
    }
    public async getGiaoVienByName(name: string): Promise<GiaoVien> {
        return await this.giaovienRepository.getGiaoVienByName(name);
    }
    public async getArrayGiaoVien(name1: string, name2: string, name3: string): Promise<GiaoVien[]> {
        return await this.giaovienRepository.getArrayGiaoVien(name1, name2, name3);
    }
    public async getGiaoVienByNameAndSalary(name: string, salary: number): Promise<GiaoVien[]> {
        return await this.giaovienRepository.getGiaoVienByNameAndSalary(name, salary);
    }
    public async getGiaoVienBySalaryAndNameOrName(salary: number, name1: string, name2: string): Promise<GiaoVien[]> {
        return await this.giaovienRepository.getGiaoVienBySalaryAndNameOrName(salary, name1, name2);
    }
    public async orderByGiaoVien(): Promise<GiaoVien[]> {
        return await this.giaovienRepository.orderByGiaoVien();
    }
    // public async groupByLuong():Promise<GiaoVien[]>{
    //     return await this.giaovienRepository.groupByLuong();
    // }


    public async limitGiaoVien(): Promise<GiaoVien[]> {
        return await this.giaovienRepository.limitGiaoVien();
    }

    public async offsetGiaoVien(): Promise<GiaoVien[]> {
        return await this.giaovienRepository.offsetGiaoVien();
    }

    public async leftJoinGiaoVienAndNguoiThan(): Promise<GiaoVien[]> {
        return await this.giaovienRepository.leftJoinGiaoVienAndNguoiThan();
    }

    public async leftJoinGVandNTnoSelect(): Promise<GiaoVien[]> {
        return await this.giaovienRepository.leftJoinGVandNTnoSelect();
    }

}