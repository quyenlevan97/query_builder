import "reflect-metadata";
import express from 'express';
import config from "./config";
import path from 'path';
import Loaders from "./loaders";
import dotenv from 'dotenv';


async function  startServer() {
    // const a= process.env.DATABASE_HOST;
    // console.log(a);
    const app=express();
    const PORT=8089;
    await Loaders({expressApp:app});
    app.listen(PORT,()=>{
        console.log(`Server listening on port : ${PORT}`);
    });
}

startServer();
