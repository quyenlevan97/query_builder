import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { GiaoVien } from "./GiaoVien";

@Entity()

export class NguoiThan {
    @PrimaryGeneratedColumn()
    MaNT: number;

    @Column()
    Ten: string;

    @ManyToOne(()=>GiaoVien,giaovien=>giaovien.Nguoithan)
    Giaovien:GiaoVien;


}