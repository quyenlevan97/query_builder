import {
    Entity, 
    PrimaryGeneratedColumn, 
    Column,
    OneToMany
} from "typeorm";

import { BoMon } from "./BoMon";


@Entity()
export class Khoa{
    @PrimaryGeneratedColumn()
    MaKhoa:number;

    @Column()
    TenKhoa:string

    @Column()
    TruongKhoa:string;

    @OneToMany (()=>BoMon,bomon=>bomon.khoa)
    boMon:BoMon[]

}