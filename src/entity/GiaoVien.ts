import {
    Entity, 
    PrimaryGeneratedColumn, 
    Column,
    CreateDateColumn,
    OneToMany,
    ManyToOne
} from "typeorm";
import { NguoiThan } from "./NguoiThan";
import { BoMon } from "./BoMon";
@Entity()
export class GiaoVien {

    @PrimaryGeneratedColumn()
    MaGV: number;

    @Column()
    Ten: string;

    @Column()
    Luong: number;

    @CreateDateColumn()
    NgaySinh: Date;


    @OneToMany(()=>NguoiThan,nguoithan=>nguoithan.Giaovien)
    Nguoithan:NguoiThan[];


    @ManyToOne(()=>BoMon,bomon=>bomon.Giaovien)
    Bomon:BoMon;


}
