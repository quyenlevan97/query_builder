import {
    Entity, 
    PrimaryGeneratedColumn, 
    Column,
    ManyToOne,
    OneToMany
} from "typeorm";
import { Khoa } from "./Khoa";
import { GiaoVien } from "./GiaoVien";
@Entity()
export class BoMon{
    @PrimaryGeneratedColumn()
    MaBM :number;

    @Column()
    TenBM:string;

    @Column()

    TruongBM:string;

    @ManyToOne(()=>Khoa,khoa=>khoa.boMon)
    khoa:Khoa

    @OneToMany(()=>GiaoVien,giaovien=>giaovien.Bomon)
    Giaovien:GiaoVien[];

}