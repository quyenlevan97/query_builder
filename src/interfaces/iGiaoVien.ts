import { BoMon } from "../entity/BoMon";

export interface IGiaoVien{
    MaGV :number;
    Ten:string;
    Luong:number;
    NgaySinh:Date;
    BoMon?: BoMon
}

export interface IGiaoVienInput{
    Ten:string;
    Luong:number;
    NgaySinh:Date;
    BoMon?: BoMon
}
export interface IGiaoVienUpdate{
    MaGV :number;
    Ten:string;
    Luong:number;
    NgaySinh:Date;
    BoMon?: BoMon
}