import { createConnection, Connection } from "typeorm";
import { Container } from 'typedi';
import { useContainer } from "typeorm";
import path from 'path';


export default async (): Promise<Connection> => {
    //TypeOrm +TypeDI
    useContainer(Container);
    const connection = await createConnection({
        type: "mysql",
        host: "localhost",
        port: 3306,
        username: "root",
        password: "",
        database: "query_builder",
        synchronize: true,
        logging: false,
        entities: [
            path.join(__dirname,'../entity/*.ts')
        ],
        bigNumberStrings: false,
    });

    return connection;
}