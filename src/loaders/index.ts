import expressLoader from './express';
import typeormLoader from './typeorm';
export default async({expressApp}:{expressApp:any})=>{
    const a= process.env.DATABASE_HOST;
    console.log("log a",a);
    await typeormLoader();
    await expressLoader({app:expressApp});
}