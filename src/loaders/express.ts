import express from 'express';
import logger from 'morgan';
import routers from '../api';


export default ({ app }: { app: express.Application }) :void=> {
    app.use(logger('dev'));
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));
    app.get("/",(req,res)=>{
        console.log("ahihihih ne");
        res.status(200).end();
    });
    app.use("/",routers());
}